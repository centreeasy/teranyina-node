#!/bin/bash
cd "/teranyina-node" # We move to the correct subfolder



if [[ -f "node-key.txt" ]]; then
    nodeK=$(cat node-key.txt | tail -n 1)
    mnemoricSeed=$(cat mnemoricSeed.txt | tail -n 1)
else
  # wget "$2" # We delete the old customSpecRaw and download it from the parameters
  ./target/release/node-template purge-chain --base-path /tmp/teranyina --chain local -y
  rm -rf /tmp/teranyina # We purge the old blockchain

  /root/.cargo/bin/subkey generate-node-key > node-key.txt 2>&1
  nodeK=$(cat node-key.txt | tail -n 1)

  mnemoricSeed=$(/root/.cargo/bin/subkey generate | head -n 1 | cut -d "\`" -f 2)

  echo "$mnemoricSeed" > mnemoricSeed.txt
fi

echo "Node key: $nodeK"
echo "Mnemoric seed: $mnemoricSeed"

./target/release/node-template key insert --base-path /tmp/teranyina --chain local --key-type aura --suri "$mnemoricSeed"
./target/release/node-template key insert --base-path /tmp/teranyina --chain local --key-type gran --suri "$mnemoricSeed" # we put our keys in the node configuration
# --node-key=cc34e0810b63f2feb10e2352a9ba0f21d7f4569b465c51ef5488be3c37c9bb09
nginx &
./target/release/node-template --base-path /tmp/teranyina --chain dev --node-key="$nodeK" --port 21000 --ws-port 22000 --rpc-port 8443 --validator --rpc-methods Unsafe  --unsafe-ws-external --unsafe-rpc-external --name teranyina --bootnodes /ip4/84.88.144.253/tcp/21000/p2p/12D3KooWL4orhdQao3iErVSGgruU9XkiwWKVZzjJutn1wKg44aAt
